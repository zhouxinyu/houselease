package com.kgc.cn.config.aop.impl;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.dto.User;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;


public class CurrentUserMsg implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        //判断所需参数是不是Object并且有没有@CurrentUser注解
        return parameter.getParameterType().isAssignableFrom(User.class) &&
                parameter.hasParameterAnnotation(CurrentUser.class);
    }

    @Override
    public User resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String userJsonStr = webRequest.getAttribute("userJsonStr", RequestAttributes.SCOPE_REQUEST).toString();
        System.out.println("userJsonStr:" + userJsonStr);
        User user = JSONObject.parseObject(userJsonStr, User.class);
        return user;
    }
}
