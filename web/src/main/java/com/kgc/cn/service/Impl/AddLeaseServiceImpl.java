package com.kgc.cn.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.kgc.cn.FeignInterface.AddLeaseFeignInterface;
import com.kgc.cn.dto.*;
import com.kgc.cn.param.*;
import com.kgc.cn.enums.AddEnum;
import com.kgc.cn.param.*;
import com.kgc.cn.service.AddLeaseService;
import com.kgc.cn.utils.redis.RedisUtils;
import com.kgc.cn.utils.returnResult.ReturnResult;
import com.kgc.cn.utils.returnResult.ReturnResultUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Ding on 2020/1/7.
 */
@Service
public class AddLeaseServiceImpl implements AddLeaseService {
    @Autowired
    private AddLeaseFeignInterface addLeaseFeign;
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 添加整租信息
     *
     * @param wholeHouseParam
     * @return
     */
    @Transactional
    @Override
    public String addNojoinLease(AddWholeHouseParam wholeHouseParam, User user) {
        HouseLeaseDto houseLeaseDto = new HouseLeaseDto();
        houseLeaseDto.setUserId(Integer.parseInt(user.getUserId()));
        // 将前台传入分别复制到想要实例
        BeanUtils.copyProperties(wholeHouseParam, houseLeaseDto);
        // 判断装修情况，改成对应id
        switch (wholeHouseParam.getDecorateSituation()) {
            case "无装修":
                houseLeaseDto.setDecorateSituationId(1);
                break;
            case "普通装修":
                houseLeaseDto.setDecorateSituationId(2);
                break;
            case "精装修":
                houseLeaseDto.setDecorateSituationId(3);
                break;
            case "豪华装修":
                houseLeaseDto.setDecorateSituationId(4);
                break;
        }
        // 获取uuid和当前日期作为房产id
        String uuid = UUID.randomUUID().toString().replace("-", "").substring(0, 10);
        String date = getDate();
        String houseId = uuid + date;
        houseLeaseDto.setHouseId(houseId);
        int result = addLeaseFeign.addNoJoinTrent(houseLeaseDto);
        // 将房产id存入redis并设置一周失效时间
        long currentTime = System.currentTimeMillis();
        String key = "updateTime:" + houseId;
        redisUtils.set(key, currentTime);
        redisUtils.set("browse:" + houseId, 0, 60 * 60 * 24 * 7);
        if (result == 1) {
            return "addSuccess";
        } else {
            return "addFail";
        }
    }

    /**
     * 房屋配置展示
     *
     * @return
     */
    @Override
    public ListParam showMerits() {
        ListParam listParam = ListParam.builder()
                .homeappliancesList(addLeaseFeign.showHouseAllocation())
                .housebenefitsList(addLeaseFeign.showBrightPoint())
                .leaserequiresList(addLeaseFeign.showRequirement())
                .build();
        return listParam;
    }

    /**
     * 中途退出保存信息
     *
     * @return
     */
    @Override
    public String addIncompleteInfo(ShowLeaseParam showLeaseParam, User user) {
        String nameSpace = "incompleteInfo:";
        String key = nameSpace + user.getUserId();
        boolean result = redisUtils.set(key, JSONObject.toJSONString(showLeaseParam), 60 * 60 * 24);
        if (result) {
            return "addSuccess";
        } else {
            return "addFail";
        }
    }

    /**
     * 继续发布信息
     *
     * @param user
     * @return
     */
    @Override
    public ShowLeaseParam continuePush(User user) {
        String nameSpace = "incompleteInfo:";
        String key = nameSpace + user.getUserId();
        if (redisUtils.hasKey(key)) {
            ShowLeaseParam param = JSONObject.parseObject(redisUtils.get(key).toString(), ShowLeaseParam.class);
            return param;
        } else {
            return null;
        }
    }

    /**
     * 获取当前时间字符串
     *
     * @return
     */
    private String getDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    /**
     * 合租
     * 添加userid和houseid
     *
     * @param rentSharingParam
     * @return
     */
    @Override
    public String addRentSharing(RentSharingParam rentSharingParam, UserParam userParam) {
        houseLeaseIsJointDto houseLeaseIsJointDto = new houseLeaseIsJointDto();
        BeanUtils.copyProperties(rentSharingParam, houseLeaseIsJointDto);
        houseLeaseIsJointDto.setUserId(123);
        //生成houseid
        String date = getDate();
        String houseId = date + rentSharingParam.getHouseName() + rentSharingParam.getHouseAddress();
        houseLeaseIsJointDto.setHouseId(houseId);
        // 将房产id存入redis并设置一周失效时间
        long currentTime = System.currentTimeMillis();
        String key = "updateTime:" + houseId;
        redisUtils.set(key, currentTime);
        redisUtils.set("browse:" + houseId, 0, 60 * 60 * 24 * 7);
        return addLeaseFeign.addRentSharing(houseLeaseIsJointDto);
    }

    /**
     * 装修查询
     * @return
     */
    @Override
    public List<DecorateSituationParam> querydecorateSituation() {
        List<DecorateSituationParam> DecorateSituationParam = Lists.newArrayList();
        List<Decoratesituation> querydecorateSituation = addLeaseFeign.querydecorateSituation();
        for (Decoratesituation decoratesituation : querydecorateSituation) {
            DecorateSituationParam decorateSituationParam = new DecorateSituationParam();
            BeanUtils.copyProperties(decoratesituation,decorateSituationParam);
            DecorateSituationParam.add(decorateSituationParam);
        }
        return DecorateSituationParam;
    }

    /**
     * 短租查询
     */
    @Override
    public List<ShortRentParam> queryShortRent() {
        List<ShortRentParam> shortRentParams = Lists.newArrayList();
        List<Lesses> ShortRentParamDto = addLeaseFeign.queryShortRent();
        for (Lesses lesses : ShortRentParamDto) {
            ShortRentParam shortRentParam = new ShortRentParam();
            BeanUtils.copyProperties(lesses,shortRentParam);
            shortRentParams.add(shortRentParam);
        }
        return shortRentParams;
    }

}
