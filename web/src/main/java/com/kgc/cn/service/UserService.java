package com.kgc.cn.service;

import com.kgc.cn.param.EnshrineParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
public interface UserService {
    //登录
    String login(String phone, String passWord, HttpServletRequest request);

    //展示收藏
    List<EnshrineParam> showEnshrine(String userId);
}
