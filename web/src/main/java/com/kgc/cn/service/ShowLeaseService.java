package com.kgc.cn.service;

import com.github.pagehelper.PageInfo;
import com.kgc.cn.dto.*;
import com.kgc.cn.param.*;
import feign.Param;
import org.apache.tomcat.jni.Address;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
public interface ShowLeaseService {
    //通过房屋id展示房屋详细信息
    ShowLeaseParam showLease(String houseId);

    // 首页展示租房信息（展示接口）
    List<homePageParam> homePage(PageLmit pageLmit);

    // 根据地区查租房信息（展示接口）
    List<homePageParam> homePageByhouseAddress(String houseArea, PageLmit pageLmit);

    // 根据小区地址和小区名称模糊搜索（展示接口）
    List<homePageParam> homePageByAddressAndName(String search, PageLmit pageLmit);

    // 根据id返回地区和价格列表
    ListByTypeIdParam ById(int id);
    // 查所有租房共同信息
    List<Houselease> houseLease(int current, int size);

    // 查所有合租信息
    List<Isjointrent> isjointrent();

    //查所有整租信息
    List<Nojointrent> nojointrent();

    // 根据地区返回共同信息
    List<Houselease> byhouseaddress(String houseArea, int current, int size);

    // 整合信息
    List<homePageParam> homepageList(List<Houselease> houseleases, List<Isjointrent> isjointrents, List<Nojointrent> nojointrents);

    // 分页
    PageBeanParam<homePageParam> Page(PageLmit pageLmit, List<homePageParam> list);

    //根据小区地址和小区名称模糊搜索
    List<Houselease> searchByAddressAndName(String search, PageLmit pageLmit);

    //展示用户下所发布的租赁信息
    PageInfo<ShowLeaseByUserParam> showLeaseByUserId(PageLmit pageLmit, String userId);

    //根据房屋id获取更新时间
    String getUpdateTime(String houseId) throws ParseException;

    //根据房屋id获取浏览人数
    int getBrowseNumber(String houseId);

    // 模糊搜索
    List<homePageParam> fuzzySearch(FuzzyHouseParam fuzzyHouseParam,@Param("sort") int sort);

    // 返回地区列表
    List<AddressesParam> addressList();

    // 返回价格列表
    List<RellsParam> rellList();

    // 返回户型列表
    List<BedRoomsParam> bedroomsList();

    // 返回朝向列表
    List<OrientationsParam> orientationsList();

    // 返回出租要求列表
    List<LeaserequiresParam> leaserequiresList();

    // 返回房源特色列表
    List<HousebenefitsParam> housebenefitsList();

    //获取轮播图
    List<PictureParam> getPicture();
}
