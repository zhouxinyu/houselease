package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Ding on 2020/1/7.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "展示租房model")
public class ShowLeaseParam {
    @ApiModelProperty(value = "房屋id")
    private String houseId;
    @ApiModelProperty(value = "房屋地址")
    private String houseAddress;
    @ApiModelProperty(value = "小区名称")
    private String houseName;
    @ApiModelProperty(value = "房屋面积单位：平方米")
    private String houseArea;
    @ApiModelProperty(value = "楼层",example = "1")
    private Integer floor;
    @ApiModelProperty(value = "装修类型名")
    private String name;
    @ApiModelProperty(value = "租赁日期id",example = "1")
    private Integer leaseId;
    @ApiModelProperty(value = "租赁单价",example = "1")
    private Integer leaseMoney;
    @ApiModelProperty(value = "联系人姓名")
    private String linkmanName;
    @ApiModelProperty(value = "联系人性别", example = "1")
    private Integer linkmanSex;
    @ApiModelProperty(value = "用户id",example = "1")
    private Integer userId;
    @ApiModelProperty(value = "看房时间")
    private String lookHouseTime;
    @ApiModelProperty(value = "适宜居住人数",example = "1")
    private Integer leaseHouseNumber;
    @ApiModelProperty(value = "租赁开始时间")
    private String leaseStartTime;
    @ApiModelProperty(value = "房屋配置：使用英文分号分割")
    private String homeAppliances;
    @ApiModelProperty(value = "房屋亮点：使用英文分号分割")
    private String houseBenefit;
    @ApiModelProperty(value = "租房要求，使用英文分号分割")
    private String leaseRequire;
    @ApiModelProperty(value = "备注")
    private String comment;
    @ApiModelProperty(value = "是否合租：0：整租；1：合租", example = "1")
    private Integer isJointRent;

    // 以下为合租的租房信息
    @ApiModelProperty(value = "户型")
    private String apartment;
    @ApiModelProperty(value = "卧室类型")
    private String bedroomType;
    @ApiModelProperty(value = "卧室朝向")
    private String bedroomOrientation;

    //一下为整租的租房信息
    @ApiModelProperty(value = "厅室",example = "1")
    private Integer hallMoney;
    @ApiModelProperty(value = "房屋朝向")
    private String houseOrientation;
    @ApiModelProperty(value = "车位")
    private String parkingLot;
    @ApiModelProperty(value = "是否有电梯：0：无；1：有",example = "1")
    private Integer isElevator;
}
