package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Ding on 2020/1/9.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("展示收藏model")
public class EnshrineParam {
    @ApiModelProperty(value = "房屋id")
    private String houseId;
    @ApiModelProperty(value = "房屋地址")
    private String houseAddress;
    @ApiModelProperty(value = "小区名字")
    private String houseName;
    @ApiModelProperty(value = "月租金",example = "1")
    private Integer leaseMoney;
    @ApiModelProperty(value = "用户id")
    private String userId;
    @ApiModelProperty(value = "是否为合租：0：整租；1：合租",example = "1")
    private Integer isJointRent;
    @ApiModelProperty(value = "卧室类型")
    private String bedRoomType;
    @ApiModelProperty(value = "房屋配置")
    private String apartment;
    @ApiModelProperty(value = "收藏时间")
    private String enshrineTime;
}
