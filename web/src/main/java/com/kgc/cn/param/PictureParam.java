package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiModel(value = "图片地址model")
@AllArgsConstructor
@NoArgsConstructor
public class PictureParam implements Serializable {
    private static final long serialVersionUID = 7112344481382230188L;
    @ApiModelProperty(value = "id",example = "1")
    private Integer id;
    @ApiModelProperty(value = "图片URL")
    private String pictureUrl;
    @ApiModelProperty(value = "图片URI")
    private String pictureUri;

}