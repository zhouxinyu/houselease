package com.kgc.cn.param;

import com.kgc.cn.dto.Homeappliances;
import com.kgc.cn.dto.Housebenefits;
import com.kgc.cn.dto.Leaserequires;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by boot on 2020/1/9
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "返回包装list的model")
public class ListParam {
    @ApiModelProperty(value = "房屋配置")
    private List<Homeappliances> homeappliancesList;
    @ApiModelProperty(value = "房屋亮点")
    private List<Housebenefits> housebenefitsList;
    @ApiModelProperty(value = "出租要求")
    private List<Leaserequires> leaserequiresList;
}
