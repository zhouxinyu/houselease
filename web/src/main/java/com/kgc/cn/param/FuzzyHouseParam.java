package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @auther zhouxinyu
 * @data 2020/1/8
 */
@Data
@ApiModel(value = "模糊查询model")
public class FuzzyHouseParam {
    @ApiModelProperty(value = "租房地址")
    private String houseAddress;
    @ApiModelProperty(value = "租房月租",example = "1")
    private Integer leaseMoney;
    @ApiModelProperty(value = "是否合租",example = "1")
    private Integer isJointRent;
    @ApiModelProperty(value = "租房亮点")
    private String houseBenefit;
    @ApiModelProperty(value = "租房要求")
    private String leaseRequire;
    @ApiModelProperty(value = "厅室情况")
    private String apartment;
    @ApiModelProperty(value = "朝向")
    private String orientation;
}
