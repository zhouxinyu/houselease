package com.kgc.cn.param;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
        import lombok.Data;

        import java.io.Serializable;

/**
 * @auther zhouxinyu
 * @data 2020/1/9
 */
@Data
@ApiOperation(value = "地区model")
public class AddressesParam implements Serializable {
    @ApiModelProperty(value = "地区id",example = "1")
    private Integer id;
    @ApiModelProperty(value = "地区名字")
    private String address;
}
