package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by boot on 2020/1/7
 */
@ApiModel(value = "整租前台信息model（全必填）")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddWholeHouseParam implements Serializable {
    @ApiModelProperty(value = "房产照片；图片地址用英文分号分隔")
    private String houseImg;
    @ApiModelProperty(value = "小区所在区域")
    private String houseAddress;
    @ApiModelProperty(value = "小区名称")
    private String houseName;
    @ApiModelProperty(value = "建筑面积")
    private String houseArea;
    @ApiModelProperty(value = "楼层", example = "1")
    private Integer floor;
    @ApiModelProperty(value = "装修情况")
    private String decorateSituation;
    @ApiModelProperty(value = "租赁日期，0为不可短租，其他数字即月数", example = "0")
    private Integer leaseId;
    @ApiModelProperty(value = "月租金", example = "1")
    private Integer leaseMoney;
    @ApiModelProperty(value = "联系人姓名")
    private String linkmanName;
    @ApiModelProperty(value = "联系人性别，1男2女", example = "1")
    private Integer linkmanSex;
    @ApiModelProperty(value = "联系人号码")
    private String linkmanPhone;
    @ApiModelProperty(value = "接听电话时间段，用用英文分号分隔")
    private String onlineTime;
    @ApiModelProperty(value = "看房时间")
    private String lookHouseTime;
    @ApiModelProperty(value = "适宜居住人数", example = "1")
    private Integer leaseHouseNumber;
    @ApiModelProperty(value = "租赁开始时间")
    private String leaseStartTime;
    @ApiModelProperty(value = "房屋配置：使用英文分号分割")
    private String homeAppliances;
    @ApiModelProperty(value = "房屋亮点：使用英文分号分割")
    private String houseBenefit;
    @ApiModelProperty(value = "租房要求，使用英文分号分割")
    private String leaseRequire;
    @ApiModelProperty(value = "补充几句")
    private String comment;

    //一下为整租的租房信息
    @ApiModelProperty(value = "厅室", example = "1")
    private Integer hallMoney;
    @ApiModelProperty(value = "房屋朝向")
    private String houseOrientation;
    @ApiModelProperty(value = "车位")
    private String parkingLot;
    @ApiModelProperty(value = "是否有电梯：0：无；1：有", example = "1")
    private Integer isElevator;
}
