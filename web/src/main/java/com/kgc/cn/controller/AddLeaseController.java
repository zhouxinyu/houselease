package com.kgc.cn.controller;

import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.dto.User;
import com.kgc.cn.enums.AddEnum;
import com.kgc.cn.param.*;
import com.kgc.cn.param.*;
import com.kgc.cn.service.Impl.AddLeaseServiceImpl;
import com.kgc.cn.utils.returnResult.ReturnResult;
import com.kgc.cn.utils.returnResult.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@Api(tags = "操作页")
@RestController
@RequestMapping(value = "/webAddLease")
public class AddLeaseController {
    @Autowired
    private AddLeaseServiceImpl addLeaseService;

    /**
     * 添加整租信息
     *
     * @param wholeHouseParam
     * @return
     */
    @ApiOperation(value = "添加整租")
    @PostMapping(value = "/addNojoin")
    @LoginRequired
    public ReturnResult addNojoin(@Valid AddWholeHouseParam wholeHouseParam,
                                  @CurrentUser User user) {
        String result = addLeaseService.addNojoinLease(wholeHouseParam, user);
        if (result.equals("addSuccess")) {
            return ReturnResultUtils.returnSuccess(result);
        } else {
            return ReturnResultUtils.returnFail(AddEnum.ADD_ERROR);
        }
    }

    /**
     * 房屋配置展示
     *
     * @return
     */
    @ApiOperation(value = "房屋配置展示")
    @GetMapping(value = "/showMerits")
    public ReturnResult<ListParam> showMerits() {
        return ReturnResultUtils.returnSuccess(addLeaseService.showMerits());
    }

    /**
     * 中途退出保存信息
     *
     * @param showLeaseParam
     * @param user
     * @return
     */
    @ApiOperation(value = "中途退出保存信息")
    @PostMapping(value = "/addIncompleteInfo")
    @LoginRequired
    public ReturnResult<String> addIncompleteInfo(@Valid ShowLeaseParam showLeaseParam,
                                                  @CurrentUser User user) {
        String result = addLeaseService.addIncompleteInfo(showLeaseParam, user);
        if (result.equals("addSuccess")) {
            return ReturnResultUtils.returnSuccess(result);
        } else {
            return ReturnResultUtils.returnFail(AddEnum.ADD_ERROR);
        }
    }

    /**
     * 继续发布信息
     *
     * @return
     */
    @ApiOperation(value = "继续发布信息")
    @PostMapping(value = "/continuePush")
    @LoginRequired
    public ReturnResult<ShowLeaseParam> continuePush(@CurrentUser User user) {
        ShowLeaseParam leaseParam = addLeaseService.continuePush(user);
        if (!ObjectUtils.isEmpty(leaseParam)) {
            return ReturnResultUtils.returnSuccess(leaseParam);
        } else {
            return ReturnResultUtils.returnFail(AddEnum.NONE_INCOMPLETEINFO);
        }
    }

    /**
     * 合租
     * @return
     */
    @PostMapping(value = "/RentSharing")
    @ApiOperation("合租")
    @LoginRequired
    public ReturnResult RentSharing(@Valid RentSharingParam rentSharingParam,@CurrentUser UserParam userParam) {
        return ReturnResultUtils.returnSuccess(addLeaseService.addRentSharing(rentSharingParam,userParam));
    }

    /**
     * 地区查找
     */
    @PostMapping(value = "/QueryDecorate")
    @ApiOperation("装修查找")
    public ReturnResult<DecorateSituationParam> querydecorateSituation() {
        return ReturnResultUtils.returnSuccess(addLeaseService.querydecorateSituation());
    }

    /**
     * 短租是否支持
     */
    @PostMapping(value = "/ShortRent")
    @ApiOperation("短租支持")
    public ReturnResult<ShortRentParam> queryShortRent() {
        return ReturnResultUtils.returnSuccess(addLeaseService.queryShortRent());
    }
}
