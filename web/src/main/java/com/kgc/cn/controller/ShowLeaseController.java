package com.kgc.cn.controller;

import com.github.pagehelper.PageInfo;
import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.dto.User;
import com.kgc.cn.enums.ShowEnum;
import com.kgc.cn.param.*;
import com.kgc.cn.service.Impl.ShowLeaseServiceImpl;
import com.kgc.cn.utils.returnResult.ReturnResult;
import com.kgc.cn.utils.returnResult.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@Api(tags = "展示租房信息")
@RestController
@RequestMapping("/house")
public class ShowLeaseController {
    @Autowired
    private ShowLeaseServiceImpl showLeaseService;

    @ApiOperation(value = "展示房屋租赁信息")
    @GetMapping(value = "/showLeaseInfo")
    public ReturnResult<ShowLeaseParam> showLeaseInfo(@ApiParam("房屋id") @RequestParam String houseId) {
        ShowLeaseParam showLeaseParam = showLeaseService.showLease(houseId);
        if (!ObjectUtils.isEmpty(showLeaseParam)) {
            return ReturnResultUtils.returnSuccess(showLeaseParam);
        }
        return ReturnResultUtils.returnFail(ShowEnum.SHOW_ERROR1);
    }

    @ApiOperation(value = "房产首页")
    @GetMapping(value = "/homepage")
    public ReturnResult<PageBeanParam<homePageParam>> homepage(@Valid PageLmit pageLmit) {
        List<homePageParam> list = showLeaseService.homePage(pageLmit);
        if (CollectionUtils.isEmpty(list)) {
            return ReturnResultUtils.returnFail(ShowEnum.SHOW_NULL);
        } else {
            return ReturnResultUtils.returnSuccess(showLeaseService.Page(pageLmit, list));
        }

    }

    @ApiOperation(value = "根据小区地址和小区名称模糊搜索")
    @PostMapping(value = "/searchByAddressAndName")
    public ReturnResult<PageBeanParam<homePageParam>> searchByAddressAndName(@ApiParam(value = "搜索关键字") @RequestParam(value = "search") String search,
                                               @Valid PageLmit pageLmit) {
        List<homePageParam> list = showLeaseService.homePageByAddressAndName(search, pageLmit);
        if (null == search) {
            return ReturnResultUtils.returnSuccess(homepage(pageLmit));
        } else {
            if (CollectionUtils.isEmpty(list)) {
                return ReturnResultUtils.returnFail(ShowEnum.SHOW_NULL);
            }
            return ReturnResultUtils.returnSuccess(showLeaseService.Page(pageLmit, list));
        }

    }

    @ApiOperation(value = "展示用户下已发布的租房信息")
    @GetMapping(value = "/showLeaseByUserId")
    @LoginRequired
    public ReturnResult<PageInfo<ShowLeaseByUserParam>> showLeaseByUserId(@Valid PageLmit pageLmit,
                                          @CurrentUser User user) {
        PageInfo<ShowLeaseByUserParam> pageInfo = showLeaseService.showLeaseByUserId(pageLmit,user.getUserId());
        if (!CollectionUtils.isEmpty(pageInfo.getList())) {
            return ReturnResultUtils.returnSuccess(pageInfo);
        }
        return ReturnResultUtils.returnFail(ShowEnum.SHOW_ERROR2);
    }

    @ApiOperation(value = "根据房屋id获取更新时间")
    @GetMapping(value = "/getUpdateTime")
    public ReturnResult<String> getUpdateTime(@ApiParam("房屋id") @RequestParam String houseId) throws ParseException {
        return ReturnResultUtils.returnSuccess(showLeaseService.getUpdateTime(houseId));
    }

    @ApiOperation(value = "根据房屋id获取浏览人数")
    @GetMapping(value = "/getBrowseNumber")
    public ReturnResult<Integer> getBrowseNumber(@ApiParam("房屋id") @RequestParam String houseId) {
        int browseNumber = showLeaseService.getBrowseNumber(houseId);
        if (browseNumber == 0) {
            return ReturnResultUtils.returnFail(ShowEnum.SHOW_ERROR3);
        }
        return ReturnResultUtils.returnSuccess(browseNumber);
    }

    @ApiOperation(value = "模糊搜索")
    @PostMapping(value = "/fuzzySearch")
    public ReturnResult<List<homePageParam>> fuzzySearch(@Valid FuzzyHouseParam fuzzyHouseParam,
                                    @ApiParam(value = "排序：1.价格正序2.价格倒序3.面积正序4.面积倒序") @RequestParam(value = "sort") int sort){
        List<homePageParam> list = showLeaseService.fuzzySearch(fuzzyHouseParam,sort);
        if(CollectionUtils.isEmpty(list)){
            return ReturnResultUtils.returnFail(ShowEnum.SHOW_NULL);
        }
        return ReturnResultUtils.returnSuccess(list);
    }

    @ApiOperation(value = "返回相对应的列表")
    @PostMapping(value = "/ById")
    public  ReturnResult<ListByTypeIdParam> ById(@ApiParam(value = "1.地区2.租金3.户型4.筛选") @RequestParam() int TypeId){
        if(ObjectUtils.isEmpty(showLeaseService.ById(TypeId))){
            return ReturnResultUtils.returnFail(ShowEnum.SHOW_HAVENO);
        }
        return ReturnResultUtils.returnSuccess(showLeaseService.ById(TypeId));
    }


    @ApiOperation(value = "获取轮播广告图")
    @GetMapping(value = "/getPicture")
    public ReturnResult<List<PictureParam>> getPicture(){
        List<PictureParam> pictureParamList = showLeaseService.getPicture();
        if (!CollectionUtils.isEmpty(pictureParamList)){
            return ReturnResultUtils.returnSuccess(pictureParamList);
        }
        return ReturnResultUtils.returnFail(ShowEnum.SHOW_ERROR4);
    }


}
