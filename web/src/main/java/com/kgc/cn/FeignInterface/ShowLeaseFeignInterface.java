package com.kgc.cn.FeignInterface;

import com.kgc.cn.FeignInterface.Impl.ShowLeaseFeignImpl;
import com.kgc.cn.dto.*;
import org.apache.tomcat.jni.Address;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@FeignClient(name = "service", fallback = ShowLeaseFeignImpl.class)
public interface ShowLeaseFeignInterface {

    @PostMapping("/show/showLease")
    ShowLeaseDto showLease(@RequestParam("houseId") String houseId);

    @GetMapping(value = "/show/houseLease")
    List<Houselease> houseLease(@RequestParam(value = "current") int current,
                                @RequestParam(value = "size") int size);

    @GetMapping(value = "/show/isjointrent")
    List<Isjointrent> isjointrent();

    @GetMapping(value = "/show/nojointrent")
    List<Nojointrent> nojointrent();

    @PostMapping(value = "/show/byhouseAddress")
    List<Houselease> byhouseaddress(@RequestParam(value = "houseArea") String houseAddress,
                                    @RequestParam(value = "current") int current,
                                    @RequestParam(value = "size") int size);

    @GetMapping(value = "/show/houseLeaseNum")
    int houseLeaseNum();

    @PostMapping(value = "/show/searchByAddressAndName")
    List<Houselease> searchByAddressAndName(@RequestParam(value = "search") String search,
                                            @RequestParam(value = "current") int current,
                                            @RequestParam(value = "size") int size);

    @PostMapping(value = "/show/showLeaseByUserId")
    List<ShowLeaseDto> showLeaseByUserId(@RequestParam("userId") String userId);

    @PostMapping(value = "/show/fuzzySearch")
    List<ShowLeaseDto> fuzzySearch(@RequestBody fuzzyHouseDto fuzzyHouseDto,
                                   @RequestParam(value = "sort") int sort);

    @PostMapping(value = "/show/getPicture")
    List<Pictures> getPicture();

    @GetMapping(value = "/show/addressList")
    List<Addresses> addressList();

    @GetMapping(value = "/show/rellList")
    List<Rells> rellList();

    @GetMapping(value = "/show/bedroomsList")
    List<Bedrooms> bedroomsList();

    @GetMapping(value = "/show/orientationsList")
    List<Orientations> orientationsList();

    @GetMapping(value = "/show/leaserequiresList")
    List<Leaserequires> leaserequiresList();

    @GetMapping(value = "/show/housebenefitsList")
    List<Housebenefits> housebenefitsList();
}
