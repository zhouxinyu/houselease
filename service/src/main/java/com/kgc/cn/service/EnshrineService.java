package com.kgc.cn.service;

import com.kgc.cn.dto.EnshrineDto;

import java.util.List;

/**
 * Created by Ding on 2020/1/9.
 */
public interface EnshrineService {
    //展示收藏
    List<EnshrineDto> showEnshrine(String userId);
}
