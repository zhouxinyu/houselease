package com.kgc.cn.service;

import com.kgc.cn.dto.Homeappliances;
import com.kgc.cn.dto.HouseLeaseDto;
import com.kgc.cn.dto.Housebenefits;
import com.kgc.cn.dto.Leaserequires;

import java.util.List;
import com.kgc.cn.dto.*;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
public interface AddLeaseService {
    // 添加整租的NoJoinHouselease和Nojointrent
    int addNojoin(HouseLeaseDto houseLeaseDto);

    // 展示可选房屋配置
    List<Homeappliances> showAllocation();

    // 展示房屋亮点
    List<Housebenefits> showPoint();

    // 展示出租要求
    List<Leaserequires> showRequirement();

    //合租
    String addRentSharing(houseLeaseIsJointDto isJointDto);

    //地区查找
    List<Decoratesituation> querydecorateSituation();

    //合租
    List<Lesses> queryShortRent();
}
