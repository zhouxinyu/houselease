package com.kgc.cn.controller;

import com.kgc.cn.dto.EnshrineDto;
import com.kgc.cn.service.EnshrineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Ding on 2020/1/9.
 */
@RestController
@RequestMapping("/enshrine")
public class EnshrineController {
    @Autowired
    private EnshrineService enshrineService;

    @PostMapping("/showEnshrine")
    public List<EnshrineDto> showEnshrine(@RequestParam("userId") String userId){
        return enshrineService.showEnshrine(userId);
    }
}
