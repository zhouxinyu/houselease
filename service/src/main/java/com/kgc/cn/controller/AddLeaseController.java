package com.kgc.cn.controller;

import com.kgc.cn.dto.Homeappliances;
import com.kgc.cn.dto.HouseLeaseDto;
import com.kgc.cn.dto.Housebenefits;
import com.kgc.cn.dto.Leaserequires;
import com.kgc.cn.dto.*;
import com.kgc.cn.service.Impl.AddLeaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@RestController
@RequestMapping(value = "/AddLease")
public class AddLeaseController {
    @Autowired
    private AddLeaseServiceImpl addLeaseService;

    /**
     * 添加整租信息
     *
     * @param houseLeaseDto
     * @return
     */
    @PostMapping(value = "/addNo")
    public int addNoJoinTrent(@RequestBody HouseLeaseDto houseLeaseDto) {
        return addLeaseService.addNojoin(houseLeaseDto);
    }

    /**
     * 展示可选房屋配置
     *
     * @return
     */
    @GetMapping(value = "/showAllocation")
    public List<Homeappliances> showHouseAllocation() {
        return addLeaseService.showAllocation();
    }

    /**
     * 展示房屋亮点
     *
     * @return
     */
    @GetMapping(value = "/showPoint")
    public List<Housebenefits> showBrightPoint() {
        return addLeaseService.showPoint();
    }

    /**
     * 展示出租要求
     *
     * @return
     */
    @GetMapping(value = "/showRequirement")
    public List<Leaserequires> showRequirement() {
        return addLeaseService.showRequirement();
    }


    /**
     * 合租
     *
     * @return
     */
    @PostMapping(value = "/RentSharing")
    public String RentSharing(@RequestBody houseLeaseIsJointDto isJointDto) {
        return addLeaseService.addRentSharing(isJointDto);
    }

    /**
     * 地区查找
     */
    @PostMapping(value = "/queryAddress")
    public List<Decoratesituation> querydecorateSituation() {
        return addLeaseService.querydecorateSituation();
    }

    /**
     * 短租
     */
    @PostMapping(value = "/queryShortRent")
    public List<Lesses> queryShortRent() {return addLeaseService.queryShortRent();}
}
