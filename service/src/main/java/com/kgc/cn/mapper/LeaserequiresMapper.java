package com.kgc.cn.mapper;

import com.kgc.cn.dto.Leaserequires;
import com.kgc.cn.dto.LeaserequiresExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LeaserequiresMapper {
    long countByExample(LeaserequiresExample example);

    int deleteByExample(LeaserequiresExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Leaserequires record);

    int insertSelective(Leaserequires record);

    List<Leaserequires> selectByExample(LeaserequiresExample example);

    Leaserequires selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Leaserequires record, @Param("example") LeaserequiresExample example);

    int updateByExample(@Param("record") Leaserequires record, @Param("example") LeaserequiresExample example);

    int updateByPrimaryKeySelective(Leaserequires record);

    int updateByPrimaryKey(Leaserequires record);
    // 返回返回租房要求列表
    List<Leaserequires> leaserequiresList();
}