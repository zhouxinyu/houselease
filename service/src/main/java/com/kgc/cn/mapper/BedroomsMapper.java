package com.kgc.cn.mapper;

import com.kgc.cn.dto.Bedrooms;
import com.kgc.cn.dto.BedroomsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BedroomsMapper {
    long countByExample(BedroomsExample example);

    int deleteByExample(BedroomsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Bedrooms record);

    int insertSelective(Bedrooms record);

    List<Bedrooms> selectByExample(BedroomsExample example);

    Bedrooms selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Bedrooms record, @Param("example") BedroomsExample example);

    int updateByExample(@Param("record") Bedrooms record, @Param("example") BedroomsExample example);

    int updateByPrimaryKeySelective(Bedrooms record);

    int updateByPrimaryKey(Bedrooms record);
    //返回所有户型（多少室）
    List<Bedrooms> bedroomsList();
}