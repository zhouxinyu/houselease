package com.kgc.cn.mapper;

import com.kgc.cn.dto.Lesses;
import com.kgc.cn.dto.LessesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LessesMapper {
    long countByExample(LessesExample example);

    int deleteByExample(LessesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Lesses record);

    int insertSelective(Lesses record);

    List<Lesses> selectByExample(LessesExample example);

    Lesses selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Lesses record, @Param("example") LessesExample example);

    int updateByExample(@Param("record") Lesses record, @Param("example") LessesExample example);

    int updateByPrimaryKeySelective(Lesses record);

    int updateByPrimaryKey(Lesses record);
    //合租
    List<Lesses> queryShortRent();
}