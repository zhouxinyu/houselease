package com.kgc.cn.enums;

/**
 * Created by Ding on 2020/1/7.
 */
public enum ShowEnum {
    SHOW_ERROR1(601, "不存在该租房信息"),
    SHOW_ERROR2(602, "没有发布过租赁信息"),
    SHOW_NULL(603, "空空如也~"),
    SHOW_ERROR3(604, "浏览人数过期!"),
    SHOW_HAVENO(605,"无此id"),
    SHOW_ERROR4(606,"查询失败!");

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    ShowEnum() {
    }

    ShowEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
