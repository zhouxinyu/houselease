package com.kgc.cn.dto;

import java.io.Serializable;

public class Isjointrent implements Serializable {
    private static final long serialVersionUID = -6023051549938370442L;
    private String houseId;

    private String apartment;

    private String bedroomType;

    private String bedroomOrientation;

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getBedroomType() {
        return bedroomType;
    }

    public void setBedroomType(String bedroomType) {
        this.bedroomType = bedroomType;
    }

    public String getBedroomOrientation() {
        return bedroomOrientation;
    }

    public void setBedroomOrientation(String bedroomOrientation) {
        this.bedroomOrientation = bedroomOrientation;
    }
}