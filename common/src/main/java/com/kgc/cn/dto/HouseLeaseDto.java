package com.kgc.cn.dto;

import java.io.Serializable;

/**
 * Created by Ding on 2020/1/7.
 */
public class HouseLeaseDto implements Serializable {
    private String houseId;
    private String houseAddress;
    private String houseName;
    private String houseArea;
    private Integer floor;
    private Integer decorateSituationId;
    private Integer leaseId;
    private Integer leaseMoney;
    private String linkmanName;
    private Integer linkmanSex;
    private Integer userId;
    private String lookHouseTime;
    private Integer leaseHouseNumber;
    private String leaseStartTime;
    private String homeAppliances;
    private String houseBenefit;
    private String leaseRequire;
    private String comment;
    private Integer isJointRent;

    //一下为整租的租房信息
    private Integer hallMoney;
    private String houseOrientation;
    private String parkingLot;
    private Integer isElevator;

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getHouseAddress() {
        return houseAddress;
    }

    public void setHouseAddress(String houseAddress) {
        this.houseAddress = houseAddress;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getHouseArea() {
        return houseArea;
    }

    public void setHouseArea(String houseArea) {
        this.houseArea = houseArea;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getDecorateSituationId() {
        return decorateSituationId;
    }

    public void setDecorateSituationId(Integer decorateSituationId) {
        this.decorateSituationId = decorateSituationId;
    }

    public Integer getLeaseId() {
        return leaseId;
    }

    public void setLeaseId(Integer leaseId) {
        this.leaseId = leaseId;
    }

    public Integer getLeaseMoney() {
        return leaseMoney;
    }

    public void setLeaseMoney(Integer leaseMoney) {
        this.leaseMoney = leaseMoney;
    }

    public String getLinkmanName() {
        return linkmanName;
    }

    public void setLinkmanName(String linkmanName) {
        this.linkmanName = linkmanName;
    }

    public Integer getLinkmanSex() {
        return linkmanSex;
    }

    public void setLinkmanSex(Integer linkmanSex) {
        this.linkmanSex = linkmanSex;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLookHouseTime() {
        return lookHouseTime;
    }

    public void setLookHouseTime(String lookHouseTime) {
        this.lookHouseTime = lookHouseTime;
    }

    public Integer getLeaseHouseNumber() {
        return leaseHouseNumber;
    }

    public void setLeaseHouseNumber(Integer leaseHouseNumber) {
        this.leaseHouseNumber = leaseHouseNumber;
    }

    public String getLeaseStartTime() {
        return leaseStartTime;
    }

    public void setLeaseStartTime(String leaseStartTime) {
        this.leaseStartTime = leaseStartTime;
    }

    public String getHomeAppliances() {
        return homeAppliances;
    }

    public void setHomeAppliances(String homeAppliances) {
        this.homeAppliances = homeAppliances;
    }

    public String getHouseBenefit() {
        return houseBenefit;
    }

    public void setHouseBenefit(String houseBenefit) {
        this.houseBenefit = houseBenefit;
    }

    public String getLeaseRequire() {
        return leaseRequire;
    }

    public void setLeaseRequire(String leaseRequire) {
        this.leaseRequire = leaseRequire;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getIsJointRent() {
        return isJointRent;
    }

    public void setIsJointRent(Integer isJointRent) {
        this.isJointRent = isJointRent;
    }

    public Integer getHallMoney() {
        return hallMoney;
    }

    public void setHallMoney(Integer hallMoney) {
        this.hallMoney = hallMoney;
    }

    public String getHouseOrientation() {
        return houseOrientation;
    }

    public void setHouseOrientation(String houseOrientation) {
        this.houseOrientation = houseOrientation;
    }

    public String getParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(String parkingLot) {
        this.parkingLot = parkingLot;
    }

    public Integer getIsElevator() {
        return isElevator;
    }

    public void setIsElevator(Integer isElevator) {
        this.isElevator = isElevator;
    }
}
