package com.kgc.cn.dto;

import java.util.ArrayList;
import java.util.List;

public class IsjointrentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public IsjointrentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andHouseIdIsNull() {
            addCriterion("houseId is null");
            return (Criteria) this;
        }

        public Criteria andHouseIdIsNotNull() {
            addCriterion("houseId is not null");
            return (Criteria) this;
        }

        public Criteria andHouseIdEqualTo(String value) {
            addCriterion("houseId =", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotEqualTo(String value) {
            addCriterion("houseId <>", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdGreaterThan(String value) {
            addCriterion("houseId >", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdGreaterThanOrEqualTo(String value) {
            addCriterion("houseId >=", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdLessThan(String value) {
            addCriterion("houseId <", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdLessThanOrEqualTo(String value) {
            addCriterion("houseId <=", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdLike(String value) {
            addCriterion("houseId like", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotLike(String value) {
            addCriterion("houseId not like", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdIn(List<String> values) {
            addCriterion("houseId in", values, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotIn(List<String> values) {
            addCriterion("houseId not in", values, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdBetween(String value1, String value2) {
            addCriterion("houseId between", value1, value2, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotBetween(String value1, String value2) {
            addCriterion("houseId not between", value1, value2, "houseId");
            return (Criteria) this;
        }

        public Criteria andApartmentIsNull() {
            addCriterion("apartment is null");
            return (Criteria) this;
        }

        public Criteria andApartmentIsNotNull() {
            addCriterion("apartment is not null");
            return (Criteria) this;
        }

        public Criteria andApartmentEqualTo(String value) {
            addCriterion("apartment =", value, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentNotEqualTo(String value) {
            addCriterion("apartment <>", value, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentGreaterThan(String value) {
            addCriterion("apartment >", value, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentGreaterThanOrEqualTo(String value) {
            addCriterion("apartment >=", value, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentLessThan(String value) {
            addCriterion("apartment <", value, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentLessThanOrEqualTo(String value) {
            addCriterion("apartment <=", value, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentLike(String value) {
            addCriterion("apartment like", value, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentNotLike(String value) {
            addCriterion("apartment not like", value, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentIn(List<String> values) {
            addCriterion("apartment in", values, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentNotIn(List<String> values) {
            addCriterion("apartment not in", values, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentBetween(String value1, String value2) {
            addCriterion("apartment between", value1, value2, "apartment");
            return (Criteria) this;
        }

        public Criteria andApartmentNotBetween(String value1, String value2) {
            addCriterion("apartment not between", value1, value2, "apartment");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeIsNull() {
            addCriterion("bedroomType is null");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeIsNotNull() {
            addCriterion("bedroomType is not null");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeEqualTo(String value) {
            addCriterion("bedroomType =", value, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeNotEqualTo(String value) {
            addCriterion("bedroomType <>", value, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeGreaterThan(String value) {
            addCriterion("bedroomType >", value, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeGreaterThanOrEqualTo(String value) {
            addCriterion("bedroomType >=", value, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeLessThan(String value) {
            addCriterion("bedroomType <", value, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeLessThanOrEqualTo(String value) {
            addCriterion("bedroomType <=", value, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeLike(String value) {
            addCriterion("bedroomType like", value, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeNotLike(String value) {
            addCriterion("bedroomType not like", value, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeIn(List<String> values) {
            addCriterion("bedroomType in", values, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeNotIn(List<String> values) {
            addCriterion("bedroomType not in", values, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeBetween(String value1, String value2) {
            addCriterion("bedroomType between", value1, value2, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomTypeNotBetween(String value1, String value2) {
            addCriterion("bedroomType not between", value1, value2, "bedroomType");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationIsNull() {
            addCriterion("bedroomOrientation is null");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationIsNotNull() {
            addCriterion("bedroomOrientation is not null");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationEqualTo(String value) {
            addCriterion("bedroomOrientation =", value, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationNotEqualTo(String value) {
            addCriterion("bedroomOrientation <>", value, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationGreaterThan(String value) {
            addCriterion("bedroomOrientation >", value, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationGreaterThanOrEqualTo(String value) {
            addCriterion("bedroomOrientation >=", value, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationLessThan(String value) {
            addCriterion("bedroomOrientation <", value, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationLessThanOrEqualTo(String value) {
            addCriterion("bedroomOrientation <=", value, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationLike(String value) {
            addCriterion("bedroomOrientation like", value, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationNotLike(String value) {
            addCriterion("bedroomOrientation not like", value, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationIn(List<String> values) {
            addCriterion("bedroomOrientation in", values, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationNotIn(List<String> values) {
            addCriterion("bedroomOrientation not in", values, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationBetween(String value1, String value2) {
            addCriterion("bedroomOrientation between", value1, value2, "bedroomOrientation");
            return (Criteria) this;
        }

        public Criteria andBedroomOrientationNotBetween(String value1, String value2) {
            addCriterion("bedroomOrientation not between", value1, value2, "bedroomOrientation");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}