package com.kgc.cn.dto;

import java.io.Serializable;

public class Nojointrent implements Serializable {
    private static final long serialVersionUID = 3979214356382212881L;
    private String houseId;

    private Integer hallMoney;

    private String houseOrientation;

    private String parkingLot;

    private Integer isElevator;

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public Integer getHallMoney() {
        return hallMoney;
    }

    public void setHallMoney(Integer hallMoney) {
        this.hallMoney = hallMoney;
    }

    public String getHouseOrientation() {
        return houseOrientation;
    }

    public void setHouseOrientation(String houseOrientation) {
        this.houseOrientation = houseOrientation;
    }

    public String getParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(String parkingLot) {
        this.parkingLot = parkingLot;
    }

    public Integer getIsElevator() {
        return isElevator;
    }

    public void setIsElevator(Integer isElevator) {
        this.isElevator = isElevator;
    }
}