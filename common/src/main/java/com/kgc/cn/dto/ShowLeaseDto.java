package com.kgc.cn.dto;

import lombok.Data;

/**
 * Created by Ding on 2020/1/7.
 */
@Data
public class ShowLeaseDto {

    private String houseId;

    private String houseAddress;

    private String houseName;

    private String houseArea;

    private Integer floor;

    private String name;

    private Integer leaseId;

    private Integer leaseMoney;

    private String linkmanName;

    private Integer linkmanSex;

    private Integer userId;

    private String lookHouseTime;

    private Integer leaseHouseNumber;

    private String leaseStartTime;

    private String homeAppliances;

    private String houseBenefit;

    private String leaseRequire;

    private String comment;

    private Integer isJointRent;

    private String apartment;

    private String bedroomType;

    private String bedroomOrientation;

    private Integer hallMoney;

    private String houseOrientation;

    private String parkingLot;

    private Integer isElevator;

}
